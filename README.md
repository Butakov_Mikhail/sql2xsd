# SQL2XSD (SQL to XSD) #
### Описание ###
Проект призван упростить создание XSD и WSDL-схем на основе SQL определений.
### Что делает ###
Программа анализирует SQL-определения процедур, объектов и таблиц вида:
```
#!sql
procedure proc_name
(
    field_in    in     varchar,     -- MinOccurs = 1
    field_out   out    number       -- MinOccurs = 1
);

create or replace type t_r_type_name is object
(
     field_name   number  -- MinOccurs = 0
);

create or replace type t_t_type_name is table of number;
```
и превращает их в определения вида:
```
#!xsd

	<xs:complexType name="ProcNameRequest">
		<xs:sequence>
			<xs:element name="fieldIn" type="xs:string" minOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ProcNameResponse">
		<xs:sequence>
			<xs:element name="fieldOut" type="xs:decimal" minOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ProcNameRequest" type="ns1:ProcNameRequest" />
	<xs:element name="ProcNameResponse" type="ns1:ProcNameResponse" />

	<xs:complexType name="TypeName">
		<xs:sequence>
			<xs:element name="fieldName" type="xs:decimal" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

```